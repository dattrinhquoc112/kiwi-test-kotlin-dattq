package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.HistoryObj
import com.example.myapplication.R
import com.squareup.picasso.Picasso

class HistoryFragmanetAdapter(val listHistory: List<HistoryObj.HistoryInfor>): RecyclerView.Adapter<HistoryFragmanetAdapter.HistoryFragmentViewHolder>() {
    class HistoryFragmentViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {
        var newsImage: ImageView = itemView.findViewById(R.id.news_image)
        var newsTitle: TextView = itemView.findViewById(R.id.news_title)
        var newsSection: TextView = itemView.findViewById(R.id.news_section)
        var newsAbstract: TextView = itemView.findViewById(R.id.news_abstract)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryFragmentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news,parent,false)
        return HistoryFragmentViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listHistory.size
    }

    override fun onBindViewHolder(holder: HistoryFragmentViewHolder, position: Int) {
        val currentHistory  = listHistory[position]
        holder.newsTitle.text = currentHistory.title
        holder.newsAbstract.text = currentHistory.abstract
        holder.newsSection.text = currentHistory.section
        Picasso.get().load(currentHistory.url).into(holder.newsImage)
    }
}
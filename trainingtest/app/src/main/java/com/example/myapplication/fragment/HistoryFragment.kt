package com.example.myapplication.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.adapter.HistoryFragmanetAdapter
import com.example.myapplication.model.HistoryObj
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentHistoryBinding


class HistoryFragment : Fragment() {

    lateinit var mBinding : FragmentHistoryBinding

    var mHistory: MutableList<HistoryObj.HistoryInfor> = mutableListOf()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e("HistoryFragment","onAttach")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.e("HistoryFragment","onCreateView")

        mBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_history,container,false)
        val adapterNews: RecyclerView = mBinding.recyclerHistory
        adapterNews.adapter = HistoryFragmanetAdapter(mHistory)
        adapterNews.layoutManager = LinearLayoutManager(context)


        return (mBinding.root)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.e("HistoryFragment","onActivityCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.e("HistoryFragment","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("HistoryFragment","onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("HistoryFragment","onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("HistoryFragment","onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.e("HistoryFragment","onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("HistoryFragment","onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.e("HistoryFragment","onDetach")
    }


}
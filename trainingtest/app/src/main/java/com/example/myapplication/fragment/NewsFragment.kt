package com.example.myapplication.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.adapter.NewsFragmentAdapter
import com.example.myapplication.model.NewsObj
import com.example.myapplication.R
import com.example.myapplication.api.NewsApi
import com.example.myapplication.databinding.FragmentNewsBinding
import com.example.myapplication.repository.Repository
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NewsFragment : Fragment(){

    lateinit var mBinding : FragmentNewsBinding

    val newsCallback = object : retrofit2.Callback<NewsObj> {
        override fun onFailure(call: Call<NewsObj>, t: Throwable) {
            Log.e("enqueue", "onFailure")
        }

        override fun onResponse(call: Call<NewsObj>, response: Response<NewsObj>) {
            if (response.isSuccessful()) {
                Log.e("enqueue", "onReponse: ${response.body()}")

                val newsObj = response.body()
                newsObj?.let {
                    val listNews = it.results
                    val adapterNews: RecyclerView = mBinding.recyclerNews
                    adapterNews.adapter = NewsFragmentAdapter(listNews as MutableList<NewsObj.NewsInfor>)
                    adapterNews.layoutManager = LinearLayoutManager(context)

                }

                Log.e("enqueue", "onReponse")

            }
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e("NewsFragment","onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("NewsFragment","onCreate")

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.e("NewsFragment","onCreateView")
        mBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_news,container,false)

        val call : Call<NewsObj> = Repository().getNewsObj()

        call.enqueue(newsCallback)

        return (mBinding.root)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.e("NewsFragment","onActivityCreated")
    }

    override fun onStart() {
        super.onStart()
        Log.e("NewsFragment","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("NewsFragment","onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("NewsFragment","onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("NewsFragment","onStop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.e("NewsFragment","onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("NewsFragment","onDestroy")
    }

    override fun onDetach() {
        super.onDetach()
        Log.e("NewsFragment","onDetach")
    }
}
package com.example.myapplication.model

data class HistoryObj (var listHistoryObj: List<HistoryInfor>) {
    data class HistoryInfor(var section: String,
                            var title: String,
                            var abstract : String,
                            var url : String)
}
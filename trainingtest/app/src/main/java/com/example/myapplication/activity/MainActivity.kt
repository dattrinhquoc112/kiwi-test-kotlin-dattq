package com.example.myapplication.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import com.example.myapplication.R
import com.example.myapplication.fragment.HistoryFragment
import com.example.myapplication.fragment.NewsFragment
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var mBinding: ActivityMainBinding
    var fm : FragmentManager = supportFragmentManager
    val newsFM = NewsFragment()
    val historyFM = HistoryFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("MainActivity","onCreate")


        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        fm.beginTransaction().add(R.id.frame_container,newsFM).addToBackStack(null).commit()
        mBinding.titleBar.text = "News"
        mBinding.newsColor.isSelected = true

        mBinding.buttonNews.setOnClickListener {
            fm.beginTransaction().replace(R.id.frame_container,newsFM).addToBackStack(null).commit()
            mBinding.titleBar.text = "News"
            mBinding.newsColor.isSelected = true
            mBinding.historyColor.isSelected = false
        }

        mBinding.buttonHistory.setOnClickListener {
            fm.beginTransaction().replace(R.id.frame_container,historyFM).addToBackStack(null).commit()

            mBinding.titleBar.text = "History"
            mBinding.historyColor.isSelected = true
            mBinding.newsColor.isSelected = false
        }

        mBinding.buttonRefresh.setOnClickListener{
            when (mBinding.historyColor.isSelected){
                true -> Toast.makeText(applicationContext,"Refresh History",Toast.LENGTH_LONG).show()
                false -> Toast.makeText(applicationContext,"Refresh News",Toast.LENGTH_LONG).show()
            }
        }

    }

    override fun onStart() {
        super.onStart()
        Log.e("MainActivity","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("MainActivity","onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("MainActivity","onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("MainActivity","onStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.e("MainActivity","onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("MainActivity","onDestroy")
    }
}
package com.example.myapplication.repository

import com.example.myapplication.api.RetrofitInstance
import com.example.myapplication.model.NewsObj
import retrofit2.Call

class Repository {
    fun getNewsObj() : Call<NewsObj> {
        return RetrofitInstance.newsApi.getNewsObj()
    }
}
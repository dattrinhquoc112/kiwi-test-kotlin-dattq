package com.example.myapplication.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NewsObj(
        @SerializedName("results")
        @Expose
        var results : List<NewsInfor>
) {
    data class NewsInfor(
            var checkSeen: Boolean = false,
            @SerializedName("section")
            @Expose
            var section : String,
            @SerializedName("title")
            @Expose
            var title : String,
            @SerializedName("abstract")
            @Expose
            var abstract : String,
            @SerializedName("multimedia")
            @Expose
            var multimedia : List<NewsImageUrl>,
            @SerializedName("short_url")
            @Expose
            var short_url : String

    ) {
        data class NewsImageUrl (
                @SerializedName("url")
                @Expose
                var url : String
        )
    }
}
package com.example.myapplication.api

import com.example.myapplication.model.NewsObj
import retrofit2.Call
import retrofit2.http.GET

interface NewsApi {
    @GET("/svc/topstories/v2/home.json?api-key=t4EG49MADcsJR9zFouDYO5ANI1rpJTAf")
    fun getNewsObj(): Call<NewsObj>
}
package com.example.myapplication.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.HistoryObj
import com.example.myapplication.model.NewsObj
import com.example.myapplication.R
import com.squareup.picasso.Picasso

class NewsFragmentAdapter(var listNews: MutableList<NewsObj.NewsInfor>): RecyclerView.Adapter<NewsFragmentAdapter.NewsFragmentViewHolder>() {

    class NewsFragmentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        var newsImage:ImageView = itemView.findViewById(R.id.news_image)
        var newsTitle:TextView = itemView.findViewById(R.id.news_title)
        var newsSection:TextView = itemView.findViewById(R.id.news_section)
        var newsAbstract:TextView = itemView.findViewById(R.id.news_abstract)
        val myWebView: WebView? = itemView.findViewById(R.id.web_view)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewsFragmentAdapter.NewsFragmentViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_news,parent,false)
        return NewsFragmentViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listNews.size
    }

    override fun onBindViewHolder(
        holder: NewsFragmentAdapter.NewsFragmentViewHolder,
        position: Int
    ) {

        val currentNews = listNews[position]
        val listNewsUrl = currentNews.multimedia[0]

        holder.newsSection.text = currentNews.section
        holder.newsTitle.text = currentNews.title
        holder.newsAbstract.text = currentNews.abstract
        Picasso.get().load(listNewsUrl.url).into(holder.newsImage)


        holder.newsImage.setOnClickListener {
            val context = holder.itemView.context
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(currentNews.short_url)
            context.startActivity(intent)
        }
    }
}